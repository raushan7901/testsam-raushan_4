﻿namespace FizzBuzz.Web.Tests.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using FizzBuzz.BusinessLayer.Interfaces;
    using FizzBuzz.Web.Controllers;
    using FizzBuzz.Web.ViewModels;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class FizzBuzzControllerTests
    {
        private Mock<IFizzBuzzService> mockFizzBuzzService;
        private FizzBuzzController fizzBuzzController;
        private FizzBuzzViewModel fizzBuzzViewModel;
        private IEnumerable<string> fizzBuzzList = new List<string>();

        [SetUp]
        public void TestSetup()
        {
            mockFizzBuzzService = new Mock<IFizzBuzzService>();
            fizzBuzzController = new FizzBuzzController(mockFizzBuzzService.Object);
        }

        [Test]
        public void FizzBuzzGet_ReturnsIndexView_OnLoading()
        {
            // Act
            ViewResult result = fizzBuzzController.Index() as ViewResult;

            // Assert
            Assert.AreEqual(result.ViewName, "Index");
        }

        [TestCase(25)]
        public void GetList_ReturnsTwentyItemsForFirstPage_WhenNumberIsGreaterThanTwenty(int number)
        {
            // Arrange
            fizzBuzzList = new List<string>() { "1", "2", "FIZZ", "4", "BUZZ", "FIZZ", "7", "8", "FIZZ", "BUZZ", "11", "FIZZ", "13", "14", "FIZZ BUZZ", "16", "17", "FIZZ", "19", "BUZZ", "FIZZ", "22", "23", "24", "BUZZ" };
            fizzBuzzViewModel = new FizzBuzzViewModel() { Number = number };
            mockFizzBuzzService.Setup(m => m.GetMessages(number)).Returns(fizzBuzzList);

            // Act
            var actResult = this.fizzBuzzController.GetList(fizzBuzzViewModel, 1) as ViewResult;
            var actualResultModel = actResult.Model as FizzBuzzViewModel;

            // Assert
            Assert.IsNotNull(actualResultModel.ResultList);
            Assert.AreEqual(20, actualResultModel.ResultList.Count());
        }

        [TestCase(25, 2)]
        public void GetList_ReturnsFiveItemsFoSecondPage_WhenNumberIsGreaterThanTwenty(int number, int pageNo)
        {
            // Arrange
            fizzBuzzList = new List<string>() { "1", "2", "FIZZ", "4", "BUZZ", "FIZZ", "7", "8", "FIZZ", "BUZZ", "11", "FIZZ", "13", "14", "FIZZ BUZZ", "16", "17", "FIZZ", "19", "BUZZ", "FIZZ", "22", "23", "24", "BUZZ" };
            fizzBuzzViewModel = new FizzBuzzViewModel() { Number = number };
            mockFizzBuzzService.Setup(m => m.GetMessages(number)).Returns(fizzBuzzList);

            // Act
            var actResult = this.fizzBuzzController.GetList(fizzBuzzViewModel, pageNo) as ViewResult;
            var actualResultModel = actResult.Model as FizzBuzzViewModel;

            // Assert
            Assert.IsNotNull(actualResultModel.ResultList);
            Assert.AreEqual(5, actualResultModel.ResultList.Count());
        }

        [TestCase(15)]
        public void GetList_ReturnsFizzBuzzDisplayText_WhenNumberProvided(int number)
        {
            // Arrange
            fizzBuzzList = new List<string>() { "1", "2", "FIZZ", "4", "BUZZ", "FIZZ", "7", "8", "FIZZ", "BUZZ", "11", "FIZZ", "13", "14", "FIZZ BUZZ" };
            fizzBuzzViewModel = new FizzBuzzViewModel() { Number = number };
            mockFizzBuzzService.Setup(m => m.GetMessages(number)).Returns(fizzBuzzList);

            // Act
            var viewResult = fizzBuzzController.GetList(fizzBuzzViewModel, 1) as ViewResult;

            // Assert
            Assert.AreEqual(viewResult.Model, fizzBuzzViewModel);
            Assert.AreEqual(viewResult.ViewName, "Index");
        }

        [TestCase(-1, false)]
        [TestCase(1500, false)]
        public void GetList_ReturnsValidationError_WhenNumberMoreThan1000AndLessThan1(int inputNumber, bool state)
        {
            // Arrange
            var viewModel = new FizzBuzzViewModel()
            {
                Number = inputNumber,
            };

            var validationResult = HelperClass.Validate(viewModel);
            foreach (var valResult in validationResult)
            {
                this.fizzBuzzController.ModelState.AddModelError(valResult.MemberNames.First(), valResult.ErrorMessage);
            }

            // Act
            var result = this.fizzBuzzController.GetList(viewModel, 1) as ViewResult;

            // Assert
            this.fizzBuzzController.ModelState.IsValid.Should().Be(state);
            result.Should().NotBeNull();
            result.Should().BeOfType(typeof(ViewResult));
            result.Model.Should().BeOfType(typeof(FizzBuzzViewModel));
        }
    }
}
