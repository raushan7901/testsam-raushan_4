﻿namespace FizzBuzz.BusinessLayer.Rules
{
    using System;
    using FizzBuzz.BusinessLayer.Interfaces;

    public class DivisibleByThreeRule : IDivisibilityRules
    {
        private readonly IDayCheckerRule dayChecker;

        public DivisibleByThreeRule(IDayCheckerRule dayChecker)
        {
            this.dayChecker = dayChecker;
        }

        public bool IsDivisible(int value)
        {
            return value % 3 == 0;
        }

        public string GetMessage()
        {
            return dayChecker.IsSpecifiedDay(DateTime.Now.DayOfWeek) ? "WIZZ" : "FIZZ";
        }
    }
}