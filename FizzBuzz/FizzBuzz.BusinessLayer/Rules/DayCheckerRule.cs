﻿namespace FizzBuzz.BusinessLayer.Rules
{
    using System;
    using FizzBuzz.BusinessLayer.Interfaces;

    public class DayCheckerRule : IDayCheckerRule
    {
        private DayOfWeek businessDay;

        public DayCheckerRule(DayOfWeek businessDay)
        {
            this.businessDay = businessDay;
        }

        public bool IsSpecifiedDay(DayOfWeek SpecifidDay)
        {
            return this.businessDay == SpecifidDay;
        }
    }
}