﻿namespace FizzBuzz.BusinessLayer.Rules
{
    using System.Collections.Generic;
    using System.Linq;
    using FizzBuzz.BusinessLayer.Interfaces;

    public class FizzBuzzService : IFizzBuzzService
    {
        private readonly IEnumerable<IDivisibilityRules> divisionList;

        public FizzBuzzService(IEnumerable<IDivisibilityRules> divisionList)
        {
            this.divisionList = divisionList;
        }

        public IEnumerable<string> GetMessages(int number)
        {
            var fizzBuzzList = new List<string>();
            for (int index = 1; index <= number; index++)
            {
                var isDivisible = this.divisionList.Where(m => m.IsDivisible(index)).ToList();
                fizzBuzzList.Add(isDivisible.Any() ? string.Join(" ", isDivisible.Select(m => m.GetMessage())) : index.ToString());
            }

            return fizzBuzzList;
        }
    }
}