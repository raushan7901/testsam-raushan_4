﻿namespace FizzBuzz.BusinessLayer.Interfaces
{
    using System.Collections.Generic;

    public interface IFizzBuzzService
    {
        IEnumerable<string> GetMessages(int number);
    }
}
