﻿namespace FizzBuzz.BusinessLayer.Interfaces
{
    public interface IDivisibilityRules
    {
        bool IsDivisible(int number);

        string GetMessage();
    }
}
