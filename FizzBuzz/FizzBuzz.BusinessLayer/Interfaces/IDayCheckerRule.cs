﻿namespace FizzBuzz.BusinessLayer.Interfaces
{
    using System;

    public interface IDayCheckerRule
    {
        bool IsSpecifiedDay(DayOfWeek businessDay);
    }
}
