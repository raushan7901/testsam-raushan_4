﻿namespace FizzBuzz.Web.ViewModels
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using PagedList;

    public class FizzBuzzViewModel
    {
        [DisplayName("Enter Number: ")]
        [Required(ErrorMessage = "Please enter a Number")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Please enter a Positive Integer")]
        [Range(1, 1000, ErrorMessage = "The input Number must be between 1 and 1000")]
        public int? Number { get; set; }

        public IPagedList<string> ResultList { get; set; }
    }
}