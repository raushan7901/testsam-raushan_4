﻿namespace FizzBuzz.Web.Controllers
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using FizzBuzz.BusinessLayer.Interfaces;
    using FizzBuzz.Web.ViewModels;
    using PagedList;

    public class FizzBuzzController : Controller
    {
        private const int PageSize = 20;

        private readonly IFizzBuzzService fizzBuzzService;

        public FizzBuzzController(IFizzBuzzService fizzBuzzService)
        {
            this.fizzBuzzService = fizzBuzzService;
        }

        [HttpGet]
        public ViewResult Index()
        {
            return this.View("Index");
        }

        public ViewResult GetList(FizzBuzzViewModel viewModel, int? page)
        {
            if (this.ModelState.IsValid)
            {
                viewModel.ResultList = this.GetValues(viewModel.Number.GetValueOrDefault()).ToPagedList(page ?? 1, PageSize);
            }

            return this.View("Index", viewModel);
        }

        private IEnumerable<string> GetValues(int number)
        {
            return this.fizzBuzzService.GetMessages(number);
        }
    }
}