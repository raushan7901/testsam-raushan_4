﻿namespace FizzBuzz.Web
{
    using System.Web;
    using System.Web.Mvc;
    using FizzBuzz.Web.ViewModels;

    public static class HtmlHelperExtensions
    {
        public static HtmlString RenderView(this HtmlHelper helper, FizzBuzzViewModel fizzBuzzViewModel)
        {
            string htmlCode = "<ul>";
            foreach (string result in fizzBuzzViewModel.ResultList)
            {
                htmlCode = htmlCode + "<li>";
                foreach (string word in result.Split(' '))
                {
                    string splitText = "<p class='" + @word + "'>" + @word + "</p>";
                    htmlCode = htmlCode + splitText + "</li>";
                }
            }

            htmlCode = htmlCode + "</ul>";
            return new HtmlString(htmlCode);
        }
    }
}