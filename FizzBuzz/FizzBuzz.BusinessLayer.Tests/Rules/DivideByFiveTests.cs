﻿namespace FizzBuzz.BusinessLayer.Tests.FizzBuzzLogic
{
    using System;
    using FizzBuzz.BusinessLayer.Interfaces;
    using FizzBuzz.BusinessLayer.Rules;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class DivideByFiveTests
    {
        private Mock<IDayCheckerRule> mockDayChecker;
        private DivisibleByFiveRule divideByFive;

        [SetUp]
        public void TestSetup()
        {
            this.mockDayChecker = new Mock<IDayCheckerRule>();
            this.divideByFive = new DivisibleByFiveRule(mockDayChecker.Object);
        }

        [TestCase(5, true)]
        public void DivisibilityByFive_ShouldReturnTrue_WhenNumberProvidedIsDivisibleByFive(int inputNumber, bool expectedResult)
        {
            // Act
            var result = divideByFive.IsDivisible(inputNumber);

            // Assert
            result.Should().Be(expectedResult);
        }

        [TestCase(7, false)]
        public void DivisibilityByFive_ShouldReturnFalse_WhenNumberProvidedIsNotDivisibleByFive(int inputNumber, bool expectedResult)
        {
            // Act
            var result = divideByFive.IsDivisible(inputNumber);

            // Assert
            result.Should().Be(expectedResult);
        }

        [TestCase(true, "WUZZ")]
        public void GetMessage_ShouldReturnWuzz_WhenDayIsSpecified(bool specifiedDay, string expectedOutput)
        {
            // Arrange
            mockDayChecker.Setup(m => m.IsSpecifiedDay(It.IsAny<DayOfWeek>())).Returns(specifiedDay);

            // Act
            var result = divideByFive.GetMessage();

            // Assert
            result.Should().Be(expectedOutput);
        }

        [TestCase(false, "BUZZ")]
        public void GetMessage_ShouldReturnBUZZ_WhenDayIsNotSpecified(bool specifiedDay, string expectedOutput)
        {
            // Arrange
            mockDayChecker.Setup(m => m.IsSpecifiedDay(It.IsAny<DayOfWeek>())).Returns(specifiedDay);

            // Act
            var result = divideByFive.GetMessage();

            // Assert
            result.Should().Be(expectedOutput);
        }
    }
}
