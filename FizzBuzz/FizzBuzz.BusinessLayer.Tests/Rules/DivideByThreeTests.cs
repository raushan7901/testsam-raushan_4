﻿namespace FizzBuzz.BusinessLayer.Tests.FizzBuzzLogic
{
    using System;
    using FizzBuzz.BusinessLayer.Interfaces;
    using FizzBuzz.BusinessLayer.Rules;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class DivideByThreeTests
    {
        private DivisibleByThreeRule divideByThree;
        private Mock<IDayCheckerRule> mockDayChecker;

        [SetUp]
        public void SetUpTest()
        {
            this.mockDayChecker = new Mock<IDayCheckerRule>();
            this.divideByThree = new DivisibleByThreeRule(mockDayChecker.Object);
        }

        [TestCase(3, true)]
        public void DivisibilityByThree_ShouldReturnTrue__WhenNumberProvidedIsDivisibleByThree(int inputNumber, bool expectedOutput)
        {
            // Act
            bool actualOutput = divideByThree.IsDivisible(inputNumber);

            // Assert
            actualOutput.Should().Be(expectedOutput);
        }

        [TestCase(7, false)]
        public void DivisibilityByThree_ShouldReturnFalse__WhenNumberProvidedIsNotDivisibleByThree(int inputNumber, bool expectedOutput)
        {
            // Act
            bool actualOutput = divideByThree.IsDivisible(inputNumber);

            // Assert
            actualOutput.Should().Be(expectedOutput);
        }

        [TestCase(true, "WIZZ")]
        public void GetMessage_ShouldReturnFizz_WhenDayIsSpecified(bool specifiedDay, string expectedOutput)
        {
            // Arrange
            mockDayChecker.Setup(m => m.IsSpecifiedDay(It.IsAny<DayOfWeek>())).Returns(specifiedDay);

            // Act
            var result = divideByThree.GetMessage();

            // Assert
            result.Should().Be(expectedOutput);
        }

        [TestCase(false, "FIZZ")]
        public void GetMessage_ShouldReturnFizz_WhenDayIsNotSpecified(bool specifiedDay, string expectedOutput)
        {
            // Arrange
            mockDayChecker.Setup(m => m.IsSpecifiedDay(It.IsAny<DayOfWeek>())).Returns(specifiedDay);

            // Act
            var result = divideByThree.GetMessage();

            // Assert
            result.Should().Be(expectedOutput);
        }
    }
}
