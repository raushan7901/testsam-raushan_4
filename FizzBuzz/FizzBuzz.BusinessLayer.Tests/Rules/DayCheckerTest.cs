﻿namespace FizzBuzz.BusinessLayer.Tests.Rules
{
    using System;
    using FizzBuzz.BusinessLayer.Interfaces;
    using FizzBuzz.BusinessLayer.Rules;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class DayCheckerTest
    {
        private DayCheckerRule dayChecker;

        [SetUp]
        public void SetUpTest()
        {
            this.dayChecker = new DayCheckerRule(DayOfWeek.Wednesday);
        }

        [TestCase("Wednesday", true)]
        [TestCase("Monday", false)]
        public void DayChecker_ReturnsTrueOrFalse_WhenDayIsSpecifiedOrNot(DayOfWeek specifiedDay, bool expectedOutput)
        {
            // Act
            var actualOutput = dayChecker.IsSpecifiedDay(specifiedDay);

            // Assert
            actualOutput.Should().Be(expectedOutput);
        }
    }
}
